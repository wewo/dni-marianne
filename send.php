<?php
define('DB_TABLE', 'md_contacts_2015');

exit_if_not_post_request();
exit_if_date_is_after_deadline();

$data = get_and_decode_data_from_post_request();
$data = htmlspecialchars_all_the_data( $data );

$sql_query = construct_sql_query_from_data( $data );
connect_to_db_and_run_query( $sql_query );


function exit_if_date_is_after_deadline() {
	if (strtotime( date('d.m.Y H:i') ) > strtotime('10.09.2015 23:59')) {
		exit;
	}
}

function exit_if_not_post_request() {
	if( $_SERVER['REQUEST_METHOD'] !== 'POST' ) {
		exit;
	}
}

function get_and_decode_data_from_post_request() {
	$data = @json_decode( $_POST['json'], true );
	if( $data === null && json_last_error() !== JSON_ERROR_NONE ) {
		exit('Wrong JSON recieved.');
	}
	return $data;
}

function htmlspecialchars_all_the_data( $data ) {
	foreach ($data as $key => $value) {
		$data[$key] = htmlspecialchars($value);
	}
	return $data;
}

function construct_sql_query_from_data( $data ) {
	$query_template	= "INSERT INTO %s (id, meno, priezvisko, ulica, cp, mesto, psc, elite_card) VALUES ('', '%s', '%s', '%s', '%s', '%s', '%s', '%s');";
	$final_query	= sprintf($query_template, DB_TABLE, $data['meno'], $data['priezvisko'], $data['ulica'], $data['cp'], $data['mesto'], $data['psc'], $data['elite-card']);
	return $final_query;
}

function connect_to_db_and_run_query( $sql_query ) {
	require_once('db.php');

	if($conn->query( $sql_query ) === false) {
		exit('false');
	}
	else {
		exit('true');
	}
}

?>