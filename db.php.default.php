<?php

// MySQL DB - mayer.sk
define('DB_SERVER',	'');
define('DB_USER',	'');
define('DB_PASS',	'');
define('DB_NAME',	'');
define('DB_PORT',	3309);

$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME, DB_PORT);
if ($conn->connect_errno) {
	echo "Error: (" . $conn->connect_errno . ") " . $conn->connect_error;
}
?>
