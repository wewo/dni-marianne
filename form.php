<?php
$valid_date = strtotime( date('d.m.Y H:i') ) < strtotime('10.09.2015 23:59');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Matus Bielik, Mayer/McCann Ericson">
	<title>MD</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/fonts.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<div class="container" id="main-container">
		<div class="row">
			<div class="col-xs-offset-2 col-xs-3">
				<div id="messages"></div>
			</div>
			<div class=" col-xs-7">
				<div id="form-holder">
					<p class="subheader">Vážení držitelia prestížnych kariet MASTERCARD<sup>®</sup></p>

					<?php if( $valid_date ): ?>
					<p>Zľavové kupóny nájdete v septembrovom vydaní Marianne, ktoré Vám bezplatne zašleme domov.<br>Stačí, ak vyplníte formulár s kontaktnými údajmi do 10. 9. 2015.</p>
					<form method="post" action="#">
						<div class="row">
							<div class="col-xs-3">
								<label for="meno">Meno:</label>
							</div>
							<div class="col-xs-9 text-input">
								<input type="text" name="meno" value="" id="meno" />
							</div>

							<div class="col-xs-3">
								<label for="priezvisko">Priezvisko:</label>
							</div>
							<div class="col-xs-9 text-input">
								<input type="text" name="priezvisko" value="" id="priezvisko" />
							</div>

							<div class="col-xs-3">
								<label for="ulica">Ulica, číslo:</label>
							</div>
							<div class="col-xs-7 text-input" style="padding-right:2px">
								<input type="text" name="ulica" value="" id="ulica" />
							</div>
							<div class="col-xs-2" style="padding-left:3px">
								<input type="text" name="cp" value="" id="cp" />
							</div>

							<div class="col-xs-3">
								<label for="mesto">Mesto:</label>
							</div>
							<div class="col-xs-9 text-input">
								<input type="text" name="mesto" value="" id="mesto" />
							</div>

							<div class="col-xs-3">
								<label for="psc">PSČ:</label>
							</div>
							<div class="col-xs-9 text-input">
								<input type="text" name="psc" value="" id="psc" />
							</div>

							<div class="col-xs-3">
								<label for="elite-card">ELITE karta:</label>
							</div>
							<div class="col-xs-9 text-input">
								<input type="text" name="elite-card" value="" id="elite-card" />
							</div>

							<div class="col-xs-offset-3 col-xs-9 submit-input">
								<input type="submit" name="send" value="Odoslať" />
							</div>
						</div>
					</form>
					<?php else: ?>

					<p>Ďakujeme, že ste využili svoju možnosť na získanie magazínu Marianne so zľavovými kupónmi v rámci lehoty do 10. 9. Prajeme príjemné nakupovanie počas Dní Marianne s MasterCard<sup>®</sup>.</p>
					<div style="text-align:center">
						<img src="/images/mastercard-logo.jpg" alt="Mastercard logo">
						<img src="/images/maestro-logo.jpg" alt="Mastercard logo">
					</div>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/validator.js"></script>

	<?php if( $valid_date ): ?>
	<script>
	$('form').on('submit', function() {
		var validationResult = validateForm();
		if(validationResult) {

			var inputs = Object();
			$('input[type="text"]').each( function(index, element) {
				inputs[$(element).attr('name')] = $(element).val();
			});

			var json_inputs = JSON.stringify(inputs);

			$.ajax({
				type: "POST",
				url: "send.php",
				data: {'json': json_inputs},
				dataType: 'text',
				success: function( data ) {
					console.log( data );
					if(data === 'true') {
						showSuccess();
					}
					else if(data === 'false') {
						console.error('Failed to save data!');
					}
				}
			});
		}

		return false;
	});

	function showSuccess() {
		$('form').remove();
		$('.subheader').remove();
		$('p').text('Ďakujeme').css('text-align', 'center');

		setTimeout( function() {
			window.location = "http://www.mastercard.sk/";
		}, 2000 );
	}
	</script>
	<?php endif; ?>
</body>
</html>