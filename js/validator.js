function validateForm() {

	var errorA = new Array();
	var error = new Object();

	if (document.getElementById("meno").value == "" /* || document.getElementById("meno").value == inputOrig['meno']*/) {
		var error = new Object();
		error.id = "meno";
		error.msg = "Prosím vyplňte meno!";
		errorA.push(error);
	}

	if (document.getElementById("priezvisko").value == "" /* || document.getElementById("priezvisko").value == inputOrig['priezvisko']*/) {
		var error = new Object();
		error.id = "priezvisko";
		error.msg = "Prosím vyplňte priezvisko! ";
		errorA.push(error);
	}

	if (document.getElementById("ulica").value == "" /* || document.getElementById("ulica").value == inputOrig['ulica']*/) {
		var error = new Object();
		error.id = "ulica";
		error.msg = "Prosím vyplňte ulicu! ";
		errorA.push(error);
	}

	if (document.getElementById("cp").value == "" /* || document.getElementById("cp").value == inputOrig['cp']*/) {
		var error = new Object();
		error.id = "cp";
		error.msg = "Prosím vyplňte číslo ulice! ";
		errorA.push(error);
	}

	if (document.getElementById("mesto").value == "" /* || document.getElementById("mesto").value == inputOrig['mesto']*/) {
		var error = new Object();
		error.id = "mesto";
		error.msg = "Prosím vyplňte mesto! ";
		errorA.push(error);
	}

	if (document.getElementById("psc").value == "" /* || document.getElementById("psc").value == inputOrig['psc']*/) {
		var error = new Object();
		error.id = "psc";
		error.msg = "Prosím vyplňte PSČ! ";
		errorA.push(error);
	}

	if (errorA.length > 0) {
		var div = document.getElementById("messages");
		var ul = document.createElement("ul");
		var li;
		var msg;
		
		while (div.childNodes[0]) {
			div.removeChild(div.childNodes[0]);
		}
		
		div.appendChild(ul);
		
		for (var i = 0; i < errorA.length; i++) {
			li = document.createElement("li");
			msg = document.createTextNode(errorA[i].msg);
			li.appendChild(msg);
			ul.appendChild(li);
		}

		return false;
	}
	else {
		return true;
	}
}

// var inputOrig = new Array();


// window.onload = function () {
// 	inputOrig['meno'] = document.getElementById("meno").value;
// 	inputOrig['priezvisko'] = document.getElementById("priezvisko").value;
// 	inputOrig['ulica'] = document.getElementById("ulica").value;
// 	inputOrig['cp'] = document.getElementById("cp").value;
// 	inputOrig['mesto'] = document.getElementById("mesto").value;
// 	inputOrig['psc'] = document.getElementById("psc").value;
	
// 	document.getElementById("meno").addEventListener('click', inputClick);
// 	document.getElementById("priezvisko").addEventListener('click', inputClick);
// 	document.getElementById("ulica").addEventListener('click', inputClick);
// 	document.getElementById("cp").addEventListener('click', inputClick);
// 	document.getElementById("mesto").addEventListener('click', inputClick);
// 	document.getElementById("psc").addEventListener('click', inputClick);
	
// 	document.getElementById("meno").addEventListener('blur', inputBlur);
// 	document.getElementById("priezvisko").addEventListener('blur', inputBlur);
// 	document.getElementById("ulica").addEventListener('blur', inputBlur);
// 	document.getElementById("cp").addEventListener('blur', inputBlur);
// 	document.getElementById("mesto").addEventListener('blur', inputBlur);
// 	document.getElementById("psc").addEventListener('blur', inputBlur);
// }

// function inputClick(e) {
// 	if (e.target.value == inputOrig[e.target.id]) {
// 		e.target.value = "";
// 	}
// }

// function inputBlur(e) {
// 	if (e.target.value == "") {
// 		e.target.value = inputOrig[e.target.id];
// 	}
// }